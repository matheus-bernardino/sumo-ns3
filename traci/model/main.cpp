#include "graph.h"

int main() {
    std::vector<std::string> lines;
    std::string s;
    while(std::getline(std::cin, s))
        lines.push_back(s);
    
    Graph G(lines, std::vector<Point>{});

    for(int i = 0; i < G.roads.size(); i++) {
        G.roads[i].cost = 69;
    }
    G.build();

    auto path = G.dijkstra("-gneE7", "gneE38");
    puts(!path.first ? "fudeu" : "sucesso");
    for(auto it : path.second)
            std::cout << it.edge_name << ' ';
    puts("");

    /*
    // <route edges="-gneE7 -gneE9 -gneE12 -gneE26 gneE11 gneE38"/>
    */
    return 0;
}