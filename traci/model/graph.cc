#include "graph.h"

Point::Point(double x, double y) {
    this->x = x;
    this->y = y;
}

Point::Point() {}

Road::Road(std::string from_name, std::string to_name, int road_index, std::string edge_name, Point from_point, Point to_point, double cost, std::vector<int> towers_order) {
    this->from_name = from_name;
    this->to_name = to_name;
    this->road_index = road_index;
    this->edge_name = edge_name;
    this->from_point = from_point;
    this->to_point = to_point;
    this->cost = cost;
    this->towers_order = towers_order;
}

Road::Road(int road_index, std::string edge_name, std::string from_name, std::string to_name) {
    this->road_index = road_index;
    this->edge_name = edge_name;
    this->from_name = from_name;
    this->to_name = to_name;
}

Road::Road() {}

Point Graph::get_mean_point(Point from_point, Point to_point) {
    return Point((from_point.x + to_point.x) / 2, (from_point.y + to_point.y) / 2);
}

Graph::Graph(std::vector<std::string> lines, std::vector<Point> towers_coodinates, std::map<uint32_t, std::vector<Road>> &m_towerToRoad) {
    std::map<std::string, Point> junctions;
    int ID = 0;

    for(std::string line : lines) {
        std::stringstream ss(line);
        std::string s, from, to, id, x, y, length;
        bool connection = false;
        while(getline(ss, s, ' ')) {
            if(s.size() >= 4 and s.substr(0, 4) == "from" and from.empty())
                from = s.substr(6, s.size() - 7);
            if(s.size() >= 2 and s.substr(0, 2) == "to" and to.empty())
                to = s.substr(4, s.size() - 5);
            if(s.size() >= 2 and s.substr(0, 2) == "id")
                id = s.substr(4, s.size() - 5);
            if(s.size() >= 2 and s[0] == 'x')
                x = s.substr(3, s.size() - 4);
            if(s.size() >= 2 and s[0] == 'y')
                y = s.substr(3, s.size() - 4);
            if(s.size() >= 11 and s.substr(1, 10) == "connection")
                connection = true;
        }
        if(connection) {
            if(from[0] == ':' or to[0] == ':') continue;
            edge_list.push_back({from, to});
            continue;
        }
        if(!from.empty() and !to.empty() and !id.empty()) {
            if(this->vertex.count(id) == 0) {
                this->roads.push_back(Road(ID, id, from, to));
                this->vertex[id] = ID++;
            }
        }
        if(!x.empty() and !y.empty() and !junctions.count(id)) {
            junctions[id] = Point(stod(x), stod(y));
        }
    }
    for(int i = 0; i < (int)this->roads.size(); i++)
    {
        Road &r = this->roads[i];
        assert(junctions.count(r.from_name) > 0);
        assert(junctions.count(r.to_name) > 0);
        r.from_point = junctions[r.from_name];
        r.to_point = junctions[r.to_name];
        Point edge_mean_point = get_mean_point(r.from_point, r.to_point);
        std::vector<int> towers_order = get_towers_order(towers_coodinates, edge_mean_point);
        r.towers_order = towers_order;
        r.cost = 1;//set_cost
        m_towerToRoad[towers_order[0]].push_back(r);
    }
    number_of_vertices = ID;
    build();
}

void Graph::build() {
    G.clear();
    G.resize(number_of_vertices);
    for(auto &it : edge_list)
        G[vertex[it[0]]].push_back({vertex[it[1]], this->roads[vertex[it[0]]].cost});
}

std::vector<int> Graph::get_towers_order(std::vector<Point> towers_coodinates, Point edge_mean_point)
{
    std::vector<std::pair<double, int>> towers_distances;
    std::vector<int> towers_id;
    int cell_id = 1;
    for(Point coordinates: towers_coodinates)
    {
        double x_sub = (edge_mean_point.x - coordinates.x);
        double y_sub = (edge_mean_point.y - coordinates.y);
        towers_distances.push_back({((x_sub * x_sub) + (y_sub * y_sub)), cell_id});
        cell_id++;
    }
    std::sort(towers_distances.begin(), towers_distances.end());
    for(auto tower : towers_distances) towers_id.push_back(tower.second);

    return towers_id;
}

Graph::Graph() {}

int Graph::get_road_index(std::string edge_name) {
    return this->vertex[edge_name];
}

std::pair<bool, std::vector<Road>> Graph::dijkstra(std::string from, std::string to) {
    std::vector<double> dist(G.size());
    std::vector<bool> processed(G.size(), false);
    std::priority_queue<std::pair<double, int>,
        std::vector<std::pair<double, int>>,
        std::greater<std::pair<double, int>>> pq;

    std::vector<int> previous(G.size(), -1);

    int v = get_road_index(from);
    int z = get_road_index(to);

    pq.push({0.0, v});
    for(u_int i = 0; i < dist.size(); i++)
        dist[i] = 1000000000.0; // INFINITO is 1000000000.0
    dist[v] = 0;

    while(!pq.empty()) {
        int u = pq.top().second;
        pq.pop();
        if(processed[u]) continue;
        processed[u] = true;
        if(u == z) break;
        for(u_int i = 0; i < G[u].size(); i++) {
            int w = G[u][i].first;
            double cost = G[u][i].second;
            assert(w < 1000000000 + 1);
            assert(dist[w] < 1000000000 + 1);
            if(dist[w] > dist[u] + cost) {
                dist[w] = dist[u] + cost;
                pq.push({dist[w], w});
                previous[w] = u;
            }
        }
    }
    if(processed[z] == false)
        return {false, std::vector<Road>{}};
    std::vector<Road> path;
    int cur_vertex = z;
    while(cur_vertex != v) {
        path.push_back(roads[cur_vertex]);
        cur_vertex = previous[cur_vertex];
    }
    path.push_back(roads[v]);
    reverse(path.begin(), path.end());
    return {true, path};
}

std::pair<bool, int > Graph::findInVector(const std::vector<Road>  & vecOfElements, const Road  & element)
{
	std::pair<bool, int > result;

	// Find given element in vector
	auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);

	if (it != vecOfElements.end())
	{
		result.second = distance(vecOfElements.begin(), it);
		result.first = true;
	}
	else
	{
		result.first = false;
		result.second = -1;
	}

	return result;
}