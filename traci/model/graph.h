#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
#include <sstream>
#include <queue>
#include <set>
#include <assert.h>

class Point {
    public:
        double x, y;
        Point(double x, double y);
        Point();
};

class Road {
    public:
        std::string from_name, to_name;
        int road_index;
        std::string edge_name;
        Point from_point, to_point;
        double cost;
        std::vector<int> towers_order;
        Road(std::string from_name, std::string to_name, int road_index, std::string edge_name, Point from_point, Point to_point, double cost, std::vector<int> towers_order);
        Road(int road_index, std::string edge_name, std::string from, std::string to);
        Road();

        bool operator==(const Road & r) const 
        {
            return r.edge_name == edge_name;
        }
};

class Graph {
    public:
        std::vector<std::vector<std::pair<int, double>>> G;
        std::vector<Road> roads;
        std::map<std::string, int> vertex;
        std::vector<std::array<std::string, 2>> edge_list;
        int number_of_vertices;
        
        std::pair<bool, std::vector<Road>>  dijkstra(std::string from, std::string to);
        std::pair<bool, int> findInVector(const std::vector<Road>  &vecOfElements, const Road  &element);
        int get_road_index(std::string edge_name);
        Point get_mean_point(Point from_point, Point to_point);
        std::vector<int> get_towers_order(std::vector<Point>, Point edge_mean_point);
        void build();
        Graph(std::vector<std::string> lines, std::vector<Point> towers_coodinates, std::map<uint32_t, std::vector<Road>> &m_towerToRoad);
        Graph();
};

#endif