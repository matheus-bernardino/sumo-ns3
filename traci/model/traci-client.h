/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

/*
 * Copyright (c) 2018 TU Dresden
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Patrick Schmager <patrick.schmager@tu-dresden.de>
 *          Sebastian Kuehlmorgen <sebastian.kuehlmorgen@tu-dresden.de>
 */

#ifndef TRACI_H
#define TRACI_H

#include <map>
#include <vector>
#include <string>
#include <functional>

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

#include "ns3/core-module.h"
#include "ns3/mobility-module.h"

#include "sumo-TraCIAPI.h"
#include "sumo-TraCIDefs.h"
#include "graph.h"
#include <algorithm>
#include "ns3/lte-module.h"

namespace ns3 {

class TraciClient : public TraCIAPI, public Object
{
public:
  // register this type with the TypeId system.
  static TypeId GetTypeId (void);

  // constructor and destructor
  TraciClient (void);
  ~TraciClient(void);

  // start up sumo; pass function pointers for including and excluding node functions
  void SumoSetup(std::function<Ptr<Node>()> includeNode, std::function<void(Ptr<Node>)> excludeNode,
                 std::map<uint32_t, uint64_t> &lastStats, std::function<void()> connectNode, std::map<uint64_t, uint64_t> &vehAppSum);

  void SumoStop();

  // get associated sumo vehicle for ns3 node
  std::string GetVehicleId(Ptr<Node> node);

  uint32_t GetVehicleMapSize(); // size of vehicle map

private:
  // perform sumo simulation for a certain time step
  void SumoSimulationStep(void);

  // get current positions from sumo vehicles and update corresponding ns3 nodes positions
  void UpdatePositions(void);

  // get new (departed) and removed (arrived) vehicles from sumo
  void GetSumoVehicles(std::vector<std::string>& sumoVehicles);

  // synchronise ns3 nodes with sumo vehicles
  void SynchroniseVehicleNodeMap(void);

  // build command line string for sumo start up
  std::string GetSumoCmdString (void);

  // map every sumo vehicle to a ns3 node
  std::map< std::string, Ptr<Node> > m_vehicleNodeMap;

  // a vehicle is untracked if it is simulated in sumo but not linked to a ns3 node because of an penetration rate < 1.0
  std::vector<std::string> m_untrackedVehicles;

  // function pointers to node include/exclude functions
  std::function<Ptr<Node>()> m_includeNode;
  std::function<void(Ptr<Node>)> m_excludeNode;
  std::function<void()> m_connectNode;

  // port handling functionality for multiple parallel simulations
  static bool PortFreeCheck (uint32_t portNum);
  static uint32_t GetFreePort (uint32_t portNum=10000);

  // update the weights of the lanes
  void UpdateTravelTimeOnRoads(void);

  // do the reroute of the vehicles
  void RerouteVehicles(void);

  // build the graph that represents the net roads
  void BuildRoadGraph(void);

  // set the coordinates of the eNB towers
  void SetTowersCoordinates(void);

  // return the current throughput normalized
  double GetNormalizedThroughput(uint64_t lowerThroughput, uint64_t upperThroughput, uint64_t currentThroughput);

  // return the current time normalized
  double GetNormalizedTime(double lowerTime, double upperTime, double currentTime);

  // return the mean of the normalized time and throughput, using the percentage of each one
  double GetMean(double value1, double value2);

  // set an additional throughput for each road of a route
  void UpdateAdditionalThroughput(std::vector<Road> &path, uint64_t bytesAplication);

  // simulation specific data members
  std::string m_sumoAddCmdOpt;
  std::string m_sumoCommand;
  std::string m_sumoConfigPath;
  std::string m_sumoBinaryPath;
  uint16_t m_sumoPort;
  bool m_sumoGUI;

  double m_penetrationRate;
  ns3::Time m_synchInterval;
  ns3::Time m_startTime;

  bool m_sumoLogFile;
  bool m_sumoStepLog;
  double m_altitude;
  int m_sumoSeed;
  ns3::Time m_sumoWaitForSocket;

  std::string m_sumoNetPath;
  // graph representing the sumo network
  Graph G;
  // interval between weight updates and reroutes
  int m_updateInterval;
  // path to the file with the coordinates of the eNB towers
  std::string m_towersPath;
  // eNB tower coordinates
  std::vector<Point> m_coordinates;
  // amount of bytes transmitted by each tower
  std::map<uint32_t, uint64_t> *m_lastStats;
  // represents the time from the last step
  int m_lastTime = -1;
  // represents the type of reroute; 0 = proposition, 1 = DSP
  uint32_t m_rerouteType = 0;
  // represents the amount of bytes to be added to the tower under the roads of a route
  std::map<std::string, uint64_t> m_additionalThroughtput;
  // the amount of bytes each vehicle sends
  std::map<uint64_t, uint64_t> m_vehAppSum;
  // maps every eNB tower to a set of roads under it
  std::map<uint32_t, std::vector<Road>> m_towerToRoad;
  // the percentage of time to be used when getting the mean between it and the throughput
  uint16_t m_timePercentage;
};

} // end namespace ns3

#endif /* TRACI_H */