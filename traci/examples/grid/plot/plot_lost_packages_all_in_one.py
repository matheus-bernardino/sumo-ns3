import sys
from bs4 import BeautifulSoup
import numpy as np
import matplotlib.pyplot as plt
import ipaddress

plt.rcParams.update({'font.size': 16})

def do_it(g, g2, g3, g4, g5, g6, g7, g8):
    x = []
    xx = []

    while(len(g) < 32):
        g.append(g[-1])
        g2.append(g2[-1])

    for i in range(len(g)):
        x.append((i)*10)

    for i in range(len(g2)):
        xx.append((i)*10)

    plt.plot(x, g, color='midnightblue',linewidth=3, label='DINO 80 Vehicles')
    plt.plot(xx, g2, linestyle='--', color='aqua',linewidth=3, label='DSP 80 Vehicles')

    plt.ylabel('Packet Delivery Rate (%)')
    plt.xlabel('Simulation Time (s)')

    x = []
    xx = []
    for i in range(len(g3)):
        x.append((i)*10)

    for i in range(len(g4)):
        xx.append((i)*10)

    plt.plot(x, g3, color='darkgreen',linewidth=3,label='DINO 160 Vehicles')
    plt.plot(xx, g4, linestyle='--', color='lightgreen',linewidth=3,label='DSP 160 Vehicles')

    x = []
    xx = []
    for i in range(len(g5)):
        x.append((i)*10)

    for i in range(len(g6)):
        xx.append((i)*10)

    plt.plot(x, g5, color='red',linewidth=3,label='DINO 200 Vehicles')
    plt.plot(xx, g6, linestyle='--', color='pink',linewidth=3,label='DSP 200 Vehicles')

    x = []
    xx = []
    for i in range(len(g7)):
        x.append((i)*10)

    for i in range(len(g8)):
        xx.append((i)*10)

    plt.plot(x, g7, color='darkorange',linewidth=3,label='DINO 300 Vehicles')
    plt.plot(xx, g8, linestyle='--', color='gold',linewidth=3,label='DSP 300 Vehicles')

    lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    axes = plt.gca()
    axes.set_xlim([0,300])
    axes.set_ylim([0,100])
    plt.yticks([10, 20, 30, 40, 50, 60, 70, 80, 90, 100])
    plt.savefig('loss_packet.pdf', format='pdf',box_extra_artists=(lgd,), bbox_inches='tight')

    plt.show()

def main(path, lost_packets_array, millisec, not_80=True):
    packets_per_sec = 1000 // int(millisec)
    last_time = 0
    from_id_to_ip = []
    from_ip_to_id = dict()

    for i in range(0, 500):
        from_id_to_ip.append(str(ipaddress.ip_address('7.0.0.2') + i))
        from_ip_to_id[str(ipaddress.ip_address('7.0.0.2') + i)] = i

    delimiter = " "
    f = open(path, "r")

    line = f.readlines()
    src = ''
    dst = ''
    count_lost = 0
    rx_packets = 0
    dropped_packets = 0
    packet_loss = []
    sum_jitter = []
    sum_delay = []
    other_packet_loss = []
    mean_lost_packets = []
    mean_dropped_packets = []
    packages_tx = 0
    packet_jitter = 0
    packet_delay = 0
    start = False
    last_delay = 0
    last_packet = 0
    id_car = 0

    arrived_time = dict()
    arrived_car = []

    for l in line:
        words = l.split(delimiter)

        if words[0] == "Step":
            last_time = int(float(words[1]))
        if words[0] == "Node" and words[2] == "chegou\n":
            id = int(words[1])
            arrived_time[id] = last_time
            arrived_car.append(id)

        if (words[0] == 'Flow' or start == True):
            start = True
            if words[0] == 'Flow':
                src = words[4] if words[3] == ';' else words[5]
                dst = words[6] if words[3] == ';' else words[8]
                id_car = from_ip_to_id[src] if src in from_ip_to_id else from_ip_to_id[dst.strip()]

            if words[0] == 'Lost':
                continue
            elif words[0] == 'Tx' and words[1] == 'Packets':
                packages_tx = int(words[3])

                if id_car in arrived_car:
                    packages_tx -= 11 * packets_per_sec if not_80 else 6 * packets_per_sec

            elif words[0] == 'Rx' and words[1] == 'Packets':
                rx_packets += int(words[3])
                if(int(words[3]) != 0):
                    other_packet_loss.append((float(words[3]) / float(packages_tx)))
                    sum_jitter.append(packet_jitter / int(words[3]))
                    sum_delay.append(packet_delay / int(words[3]))
            elif words[0] == 'Packet':
                continue
            elif words[0] == 'Dropped':
                continue
            elif words[0] == 'Sum':
                continue
            elif words[0] == 'Delay':
                packet_delay = int(words[3][1:-5]) / 1000000
            elif words[0] == 'Rx' or words[0] == 'Tx' or words[0] == 'Flow' or words[0] == '---------------------------------------------------------------------------\n' or words[0] == 'Duration:' or words[0] == 'Last' or words[0] == 'Throughput:':
                continue
            else:
                lost_packets_array.append((float(np.mean(other_packet_loss)))*100)
                last_delay = float(np.mean(sum_delay))
                last_packet = (float(np.mean(other_packet_loss)))*100

                src = ''
                count_lost = 0
                rx_packets = 0
                dropped_packets = 0
                packet_loss = []
                sum_jitter = []
                sum_delay = []
                other_packet_loss = []
                mean_lost_packets = []
                mean_dropped_packets = []
                packages_tx = 0
                packet_jitter = 0
                packet_delay = 0
                start = False

'''
Receive as parameters the two different algorithms logs for all the four scenarios.
The order should be first the proposition and the second the base algorithm for each scenario.
'''
if __name__ == "__main__":
    lost_packets_array_1 = []
    lost_packets_array_2 = []
    lost_packets_array_3 = []
    lost_packets_array_4 = []
    lost_packets_array_5 = []
    lost_packets_array_6 = []
    lost_packets_array_7 = []
    lost_packets_array_8 = []

    main(sys.argv[1], lost_packets_array_1, sys.argv[9], False)
    main(sys.argv[2], lost_packets_array_2, sys.argv[9], False)
    main(sys.argv[3], lost_packets_array_3, sys.argv[9])
    main(sys.argv[4], lost_packets_array_4, sys.argv[9])
    main(sys.argv[5], lost_packets_array_5, sys.argv[9])
    main(sys.argv[6], lost_packets_array_6, sys.argv[9])
    main(sys.argv[7], lost_packets_array_7, sys.argv[9])
    main(sys.argv[8], lost_packets_array_8, sys.argv[9])
    do_it(lost_packets_array_1, lost_packets_array_2, lost_packets_array_3, lost_packets_array_4, lost_packets_array_5, lost_packets_array_6, lost_packets_array_7, lost_packets_array_8)