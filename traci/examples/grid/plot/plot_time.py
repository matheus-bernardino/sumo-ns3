import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 22})

def main():

    labels = ['80 Veículos', '160 Veículos', '200 Veículos', '300 Veículos']
    proposition = [83.4, 96.6, 113.6, 124.9]
    travel_time = [79.4, 112.0, 137.0, 134.7]

    x = np.arange(len(labels))  # the label locations
    width = 0.4  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, proposition, width, label='Proposta')
    rects2 = ax.bar(x + width/2, travel_time, width, label='DSP')
    plt.gcf().set_size_inches(12.3, 9)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Duração Média de Percurso (s)')
    # ax.set_title('Scores by group and gender')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
           ncol=2, borderaxespad=0.)

    autolabel(rects1, ax)
    autolabel(rects2, ax)

    fig.tight_layout()
    plt.savefig('tempo.pdf', format='pdf')
    plt.show()

def autolabel(rects, ax):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

if __name__ == "__main__":
	main()