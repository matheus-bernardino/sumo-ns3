#!/bin/bash

nohup ./waf --run "lte --numBearersPerUe=1 --numberOfUes=160 --simTime=320
--towersPath=./src/traci-applications/examples/7x7towers-Copy1 --netPath=./src/traci/examples/grid/7x7.net.xml
--sumoConfigPath=./src/traci/examples/grid/7x7_160_Veiculos/7x7.160.10.1.sumo.cfg --updateIntervalTime=10 --dlPdcpOutputFilename=DlPdcpStats.160.10.1.txt
--ulPdcpOutputFilename=UlPdcpStats.160.10.1.txt --dlRlcOutputFilename=DlRlcStats.160.10.1.txt --ulRlcpOutputFilename=UlRlcStats.160.10.1.txt
--rerouteType=1 --pdcpFile=./DlPdcpStats.160.10.1.txt --routesFile=./src/traci/examples/grid/routes160.xml --first=true" &> nohup.160.10.1.out

nohup ./waf --run "lte --numBearersPerUe=1 --numberOfUes=160 --simTime=320
--towersPath=./src/traci-applications/examples/7x7towers-Copy1 --netPath=./src/traci/examples/grid/7x7.net.xml
--sumoConfigPath=./src/traci/examples/grid/7x7_160_Veiculos/7x7.160.10.1.sumo.cfg --updateIntervalTime=10 --dlPdcpOutputFilename=DlPdcpStats.160.10.1.2.txt
--ulPdcpOutputFilename=UlPdcpStats.160.10.1.2.txt --dlRlcOutputFilename=DlRlcStats.160.10.1.2.txt --ulRlcpOutputFilename=UlRlcStats.160.10.1.2.txt
--rerouteType=1 --pdcpFile=./DlPdcpStats.160.10.1.txt --routesFile=./src/traci/examples/grid/routes160.xml --first=false" &> nohup.160.10.1.2.out
