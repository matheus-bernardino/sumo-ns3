#!/bin/bash

nohup ./waf --run "lte --numBearersPerUe=1 --numberOfUes=160 --simTime=320
--towersPath=./src/traci-applications/examples/7x7towers-Copy1 --netPath=./src/traci/examples/grid/7x7.net.xml
--sumoConfigPath=./src/traci/examples/grid/7x7_160_Veiculos/7x7.160.20.0.sumo.cfg --updateIntervalTime=20 --dlPdcpOutputFilename=DlPdcpStats.160.20.0.txt
--ulPdcpOutputFilename=UlPdcpStats.160.20.0.txt --dlRlcOutputFilename=DlRlcStats.160.20.0.txt --ulRlcpOutputFilename=UlRlcStats.160.20.0.txt
--rerouteType=0 --pdcpFile=./DlPdcpStats.160.20.0.txt --routesFile=./src/traci/examples/grid/routes160.xml --first=true" &> nohup.160.20.0.out

nohup ./waf --run "lte --numBearersPerUe=1 --numberOfUes=160 --simTime=320
--towersPath=./src/traci-applications/examples/7x7towers-Copy1 --netPath=./src/traci/examples/grid/7x7.net.xml
--sumoConfigPath=./src/traci/examples/grid/7x7_160_Veiculos/7x7.160.20.0.sumo.cfg --updateIntervalTime=20 --dlPdcpOutputFilename=DlPdcpStats.160.20.0.2.txt
--ulPdcpOutputFilename=UlPdcpStats.160.20.0.2.txt --dlRlcOutputFilename=DlRlcStats.160.20.0.2.txt --ulRlcpOutputFilename=UlRlcStats.160.20.0.2.txt
--rerouteType=0 --pdcpFile=./DlPdcpStats.160.20.0.txt --routesFile=./src/traci/examples/grid/routes160.xml --first=false" &> nohup.160.20.0.2.out
