/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Manuel Requena <manuel.requena@cttc.es>
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store-module.h"

#include "ns3/ns2-mobility-helper.h"
#include "ns3/netanim-module.h"

#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor-module.h"

#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <random>

#include "ns3/traci-module.h"
#include "ns3/traci-applications-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LTE");

// Prints actual position and velocity when a course change event occurs
void
CourseChange (std::ostream *os, std::string foo, Ptr<const MobilityModel> mobility)
{
  Vector pos = mobility->GetPosition (); // Get position
  Vector vel = mobility->GetVelocity (); // Get velocity

  // Prints position and velocities
  if("/NodeList/0/$ns3::MobilityModel/CourseChange" == foo)
    *os << Simulator::Now () << " " << foo << " POS: x=" << pos.x << ", y=" << pos.y
        << ", z=" << pos.z << "; VEL:" << vel.x << ", y=" << vel.y
        << ", z=" << vel.z << std::endl;
}

std::map<u_int64_t, std::pair<u_int64_t, u_int64_t>> ueEnb;

void
NotifyHandoverStartUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellid,
                       uint16_t rnti,
                       uint16_t targetCellId)
{
  std::cout << Simulator::Now ().GetSeconds () << " Handover Start Ue: "
  // std::cout << context
            << " UE IMSI " << imsi
            << ": previously connected to CellId " << cellid
            << " with RNTI " << rnti
            << ", doing handover to CellId " << targetCellId
            << std::endl;
}

void
NotifyHandoverEndOkUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellid,
                       uint16_t rnti)
{
  ueEnb[imsi] = {cellid, rnti};
  std::cout << context
            << " UE IMSI " << imsi
            << ": successful handover to CellId " << cellid
            << " with RNTI " << rnti
            << std::endl;
}

void
NotifyConnectionEstablishedUe (std::string context,
                               uint64_t imsi,
                               uint16_t cellid,
                               uint16_t rnti)
{
  ueEnb[imsi] = {cellid, rnti};
  std::cout << context
            << " UE IMSI " << imsi
            << ": connected to CellId " << cellid
            << " with RNTI " << rnti
            << std::endl;
}

void
NotifyConnectionEstablishedEnb (std::string context,
                                uint64_t imsi,
                                uint16_t cellid,
                                uint16_t rnti)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": successful connection of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;
}

void
NotifyHandoverStartEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti,
                        uint16_t targetCellId)
{
  std::cout << Simulator::Now ().GetSeconds () << " " << "Handover Start eNB: "
  // std::cout << context
            << " eNB CellId " << cellid
            << ": start handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << " to CellId " << targetCellId
            << std::endl;
}

void
NotifyHandoverEndOkEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": completed handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;
}

std::map<int, int>
GetStartTime(std::string path)
{
	std::string s;
	std::map<int, int> mapa;
  freopen(path.c_str(), "r", stdin);
    std::cin.clear();
    std::cin.ignore();
  int count = 0;
	while(getline(std::cin, s))
	{
    int id = -1;
		std::string aux;
		std::string depart, arrival;
		std::stringstream ss(s);
		do
		{
			ss >> aux;
			if(aux.substr(0, 2) == "id")
      {
         id = count++;
      }
			if(aux.substr(0, 6) == "depart")
      {
        depart = aux.substr(8, aux.size() - 10);
        break;
      }
		}while(ss);

    if(id == -1 or depart.empty()) continue;
    mapa[id] = stoi(depart);
	}

	return mapa;
}

std::map<int, int>
GetStopTime(std::string path)
{
    bool flag = false;
    std::string s;
	std::map<int, int> mapa;
    freopen(path.c_str(), "r", stdin);
    std::cin.clear();
    std::cin.ignore();
	while(getline(std::cin, s))
	{
		std::string aux;
    std::vector<std::string> strs;
		std::stringstream ss(s);
		do
		{
			ss >> aux;
      strs.push_back(aux);
		}while(ss);
		if(flag == true)
            mapa[stoi(strs[3]) - 1] = stoi(strs[1]);
        flag = true;
	}

	return mapa;
}

std::vector<Vector>
ProcessStart(std::string mobilityPath, int size)
{
  int count = 0;
  std::vector<bool> ids;
  ids.resize(size);
  std::ifstream file(mobilityPath);
  std::vector<Vector> uePositions;
  if (file.is_open())
  {
    std::string line;
    while (getline(file, line)) {
        std::string::size_type n = line.find("setdest");
        if(std::string::npos != n)
        {
          std::string aux;
      		std::string id, x, y, z;
          std::stringstream ss(line);
          do
          { ss >> aux;
            if(aux.substr(1, 6) == "$node_")
            {
              id = aux.substr(8, aux.size() - 9);
              ss >> aux;
              ss >> aux;
              x = aux;
              ss >> aux;
              y = aux;
              ss >> aux;
              z = aux.substr(0, aux.size() - 1);

              if(ids[stoi(id)] == false)
              {
                ids[stoi(id)] = true;
                Vector uePosition (stod(x), stod(y), stod(x));
                uePositions.push_back(uePosition);
                count++;
              }
            }
          }while(ss);
        }
        if(count == size) break;
    }
    file.close();
  }

  return uePositions;
}

void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon)
{
  flowMon->CheckForLostPackets();
	std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
	Ptr<Ipv4FlowClassifier> classing = DynamicCast<Ipv4FlowClassifier> (fmhelper->GetClassifier());
	for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
	{
		Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);

    NS_LOG_UNCOND("Flow ID: " << stats->first <<" ; "<< fiveTuple.sourceAddress <<" -----> "<<fiveTuple.destinationAddress);
		NS_LOG_UNCOND("Duration: "<<stats->second.timeLastTxPacket.GetSeconds()-stats->second.timeFirstTxPacket.GetSeconds());
    NS_LOG_UNCOND("Tx Packets = " << stats->second.txPackets);
		NS_LOG_UNCOND("Tx Bytes = " << stats->second.txBytes);
    NS_LOG_UNCOND("Sum jitter = "<<stats->second.jitterSum);
    NS_LOG_UNCOND("Delay Sum = "<<stats->second.delaySum);
    NS_LOG_UNCOND("Lost Packet: "<< stats->second.lostPackets);
    NS_LOG_UNCOND("Dropped Packet = " << stats->second.packetsDropped.size());
    NS_LOG_UNCOND("Rx Packets = " << stats->second.rxPackets);
		NS_LOG_UNCOND("Rx Bytes = " << stats->second.rxBytes);
		NS_LOG_UNCOND("Last Received Packet: "<< stats->second.timeLastTxPacket.GetSeconds()<<" Seconds");
		NS_LOG_UNCOND("Throughput: "<< stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds()-stats->second.timeFirstRxPacket.GetSeconds()) / 1048576  << " Mbps");
    NS_LOG_UNCOND("Packet loss %= "<<(((stats->second.txPackets-stats->second.rxPackets)*1.0)/stats->second.txPackets));
		NS_LOG_UNCOND("---------------------------------------------------------------------------");
	}
	Simulator::Schedule(Seconds(10), &ThroughputMonitor, fmhelper, flowMon);
}

  void desconnectNode(Ptr<ns3::Node> exNode)
  {
      Ptr<ConstantPositionMobilityModel> mob = exNode->GetObject<ConstantPositionMobilityModel>();
      mob->SetPosition(Vector(-1000000.0,-1000000.0,250.0));// rand() for visualization purposes
  }

/**
 * Sample simulation script for an automatic X2-based handover based on the RSRQ measures.
 * It instantiates two eNodeB, attaches one UE to the 'source' eNB.
 * The UE moves between both eNBs, it reports measures to the serving eNB and
 * the 'source' (serving) eNB triggers the handover of the UE towards
 * the 'target' eNB when it considers it is a better eNB.
 */
int
main (int argc, char *argv[])
{
  /*
     Logs that can be enabled
  */

  // LogLevel logLevel = (LogLevel)(LOG_PREFIX_ALL | LOG_LEVEL_ALL);

  // LogComponentEnable ("LteHelper", logLevel);
  // LogComponentEnable ("EpcHelper", logLevel);
  // LogComponentEnable ("EpcEnbApplication", logLevel);
  // LogComponentEnable ("EpcX2", logLevel);
  // LogComponentEnable ("EpcSgwPgwApplication", logLevel);

  // LogComponentEnable ("LteEnbRrc", logLevel);
  // LogComponentEnable ("LteEnbNetDevice", logLevel);
  // LogComponentEnable ("LteUeRrc", logLevel);
  // LogComponentEnable ("LteUeNetDevice", logLevel);
  // LogComponentEnable ("A2A4RsrqHandoverAlgorithm", logLevel);
  // LogComponentEnable ("A3RsrpHandoverAlgorithm", logLevel);

  // Notification of the passing packets
  // LogComponentEnable ("UdpClient", LOG_LEVEL_INFO);
  // LogComponentEnable ("UdpServer", LOG_LEVEL_INFO);
  // LogComponentEnable ("EpcMmeApplication", LOG_LEVEL_DEBUG);
  // LogComponentEnable ("EpcMmeApplication", LOG_LEVEL_INFO);
  // LogComponentEnable ("EpcPgwApplication", LOG_LEVEL_DEBUG);
  // LogComponentEnable ("EpcSgwApplication", LOG_LEVEL_DEBUG);

  uint16_t rerouteType = 0;
  uint16_t updateIntervalTime = 10;
  uint16_t numberOfUes = 300;
  uint16_t numBearersPerUe = 1;
  double speed = 20;
  double simTime = 340;
  double enbTxPowerDbm = 46.0;
  uint16_t timePercentage = 50;

  std::string logFile = "/home/$USER/workspace/ns-allinone-3.30/ns-3.30/scratch/main-ns2-mob.log";
  std::string routesFile = "./src/traci/examples/grid/300s/routes300.xml";
  std::string towersPath = "./src/traci-applications/examples/16x16towers";
  std::string netPath = "./src/traci/examples/grid/16x16.net.xml";
  std::string mobilityPath = "./src/traci/examples/grid/mobility.tcl";
  std::string sumoConfigPath = "./src/traci/examples/grid/300s/16x16.300.10.0.sumo.cfg";
  std::string dlPdcpOutputFilename = "DlPdcpStats.300.10.0.txt";
  std::string ulPdcpOutputFilename = "UlPdcpStats.300.10.0.txt";
  std::string dlRlcOutputFilename = "DlRlcStats.300.10.0.txt";
  std::string ulRlcpOutputFilename = "UlRlcStats.300.10.0.txt";
  std::string pdcpFile = "./DlPdcpStats.300.10.0.txt";

  bool first = true;

  std::map<uint64_t, uint64_t> vehAppSum;

  // change some default attributes so that they are reasonable for
  // this scenario, but do this before processing command line
  // arguments, so that the user is allowed to override these settings

  Config::SetDefault ("ns3::LteHelper::UseIdealRrc", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));

  // Set a value to the NumberOfRaPreambles so it doesn't have a HO Preparation Failure
  Config::SetDefault ("ns3::LteEnbMac::NumberOfRaPreambles", UintegerValue (30));

  // Command line arguments
  CommandLine cmd;
  cmd.AddValue ("simTime", "Total duration of the simulation (in seconds)", simTime);
  cmd.AddValue ("speed", "Speed of the UE (default = 20 m/s)", speed);
  cmd.AddValue ("enbTxPowerDbm", "TX power [dBm] used by HeNBs (default = 46.0)", enbTxPowerDbm);

  cmd.AddValue ("numBearersPerUe", "Number of Beares per Ues nodes (default = 1)", numBearersPerUe);
  cmd.AddValue ("routesFile", "SUMO vehicle movement trace file", routesFile);
  cmd.AddValue ("logFile", "Log file", logFile);
  cmd.AddValue ("numberOfUes", "Number of Ues nodes (default = 1)", numberOfUes);
  cmd.AddValue ("towersPath", "Path to the file with the eNBs coordinates", towersPath);
  cmd.AddValue ("netPath", "Path to the SUMO net file", netPath);
  cmd.AddValue ("mobilityPath", "Path to the NS mobility file", mobilityPath);
  cmd.AddValue ("sumoConfigPath", "Path to the SUMO configuration file", sumoConfigPath);
  cmd.AddValue ("updateIntervalTime", "Interval time to update the travel time on roads and reroute (default = 0)", updateIntervalTime);
  cmd.AddValue ("dlPdcpOutputFilename", "Name of the file with downlink PDCP output", dlPdcpOutputFilename);
  cmd.AddValue ("ulPdcpOutputFilename", "Name of the file with uplink PDCP output", ulPdcpOutputFilename);
  cmd.AddValue ("dlRlcOutputFilename", "Name of the file with downlink RLC output", dlRlcOutputFilename);
  cmd.AddValue ("ulRlcpOutputFilename", "Name of the file with uplink RLC output", ulRlcpOutputFilename);
  cmd.AddValue ("pdcpFile", "Pdcp path file", pdcpFile);
  cmd.AddValue ("rerouteType", "Set the type of reroute used (default = 0)", rerouteType);
  cmd.AddValue ("first", "Flag to set if is the first run (default = true)", first);
  cmd.AddValue ("timePercentage", "The use percentage of time", timePercentage);
  cmd.Parse (argc, argv);

  // Get the start and stop times
  std::map<int, int> stopTimes = GetStopTime(pdcpFile);
  // can be used when vehicles starts moving at any point of the simulation
  // std::map<int, int> startTimes = GetStartTime(routesFile);

  // Open log file for output
  std::ofstream os;
  os.open (logFile.c_str ());

  NodeContainer ueNodes;
  ueNodes.Create (numberOfUes);
  uint32_t nodeCounter (0);
  std::cin.clear();
  std::cin.ignore();
  std::vector<Vector> uePositionAlloc = ProcessStart(mobilityPath, numberOfUes);
  MobilityHelper mobility;
  Ptr<UniformDiscPositionAllocator> positionAlloc = CreateObject<UniformDiscPositionAllocator> ();
  positionAlloc->SetX (320.0);
  positionAlloc->SetY (320.0);
  positionAlloc->SetRho (25.0);
  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (ueNodes);

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");

  /*
     A2A4 handover algorithm can also be used here
  */
  // lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  // lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold",
  //                                           UintegerValue (30));
  // lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset",
  //                                           UintegerValue (1));
  lteHelper->SetHandoverAlgorithmType ("ns3::A3RsrpHandoverAlgorithm");
  lteHelper->SetHandoverAlgorithmAttribute ("Hysteresis",
                                            DoubleValue (3.0));
  lteHelper->SetHandoverAlgorithmAttribute ("TimeToTrigger",
                                             TimeValue (MilliSeconds (80)));

  /*
     SUMO client parameters
  */
  Ptr<TraciClient> sumoClient = CreateObject<TraciClient> ();
  sumoClient->SetAttribute ("SumoConfigPath", StringValue (sumoConfigPath));
  sumoClient->SetAttribute ("SumoBinaryPath", StringValue (""));    // use system installation of sumo
  sumoClient->SetAttribute ("SynchInterval", TimeValue (Seconds (0.1)));
  sumoClient->SetAttribute ("StartTime", TimeValue (Seconds (0.0)));
  sumoClient->SetAttribute ("SumoGUI", BooleanValue (false));
  sumoClient->SetAttribute ("SumoPort", UintegerValue (3400));
  sumoClient->SetAttribute ("PenetrationRate", DoubleValue (1.0));  // portion of vehicles equipped with wifi
  sumoClient->SetAttribute ("SumoLogFile", BooleanValue (true));
  sumoClient->SetAttribute ("SumoStepLog", BooleanValue (false));
  sumoClient->SetAttribute ("SumoSeed", IntegerValue (10));
  sumoClient->SetAttribute ("SumoAdditionalCmdOptions", StringValue ("--verbose true"));
  sumoClient->SetAttribute ("SumoWaitForSocket", TimeValue (Seconds (1.0)));
  sumoClient->SetAttribute ("SumoNetPath", StringValue (netPath));
  sumoClient->SetAttribute ("UpdateInterval", IntegerValue (updateIntervalTime));
  sumoClient->SetAttribute ("TowersCoordinatesPath",StringValue(towersPath));
  sumoClient->SetAttribute ("RerouteType", UintegerValue (rerouteType));
  sumoClient->SetAttribute ("TimePercentage", UintegerValue ( timePercentage ));
  /*
     9. Setup interface and application for dynamic nodes
     Parameter not used in this scenario
  */
  // VehicleSpeedControlHelper vehicleSpeedControlHelper (9);
  // vehicleSpeedControlHelper.SetAttribute ("Client", (PointerValue) sumoClient); // pass TraciClient object for accessing sumo in application


  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create the Internet
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);


  // Routing of the Internet Host (towards the LTE network)
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  // interface 0 is localhost, 1 is the p2p device
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  /*
   * Base topology, not used in this scenario
   * Network topology:
   *
   *      |     + --------------------------------------------------------->
   *      |     UE
   *      |
   *      |               d                   d                   d
   *    y |     |-------------------x-------------------x-------------------
   *      |     |                 eNodeB              eNodeB
   *      |   d |
   *      |     |
   *      |     |                                             d = distance
   *            o (0, 0, 0)                                   y = yForUe
   */

  std::string s;
  std::vector<std::pair<int, int>> coordinates;
  freopen(towersPath.c_str(), "r", stdin);
  std::cin.clear();
  while(getline(std::cin, s))
	{
		std::string aux;
    int x, y;
		std::stringstream ss(s);
    ss >> aux;
    x = stoi(aux);
    ss >> aux;
    y = stoi(aux);
		coordinates.push_back({x, y});
	}

  // NodeContainer ueNodes;
  NodeContainer enbNodes;
  enbNodes.Create (coordinates.size());

  // Install Mobility Model in eNB
  std::cout << "eNB tower positions:" << std::endl;
  Ptr<ListPositionAllocator> enbPositionAlloc = CreateObject<ListPositionAllocator> ();
  for (uint16_t i = 0; i < coordinates.size(); i++)
  {
    std::cout << "x: " << coordinates[i].first << " y: " << coordinates[i].second << std::endl;
    Vector enbPosition (coordinates[i].first, coordinates[i].second, 0);
    enbPositionAlloc->Add (enbPosition);
  }

  MobilityHelper enbMobility;
  enbMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  enbMobility.SetPositionAllocator (enbPositionAlloc);
  enbMobility.Install (enbNodes);

  // Install LTE Devices in eNB and UEs
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (enbTxPowerDbm));
  Config::SetDefault ("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (320));
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install the IP stack on the UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIfaces;
  ueIpIfaces = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  NS_LOG_LOGIC ("setting up applications");

  /*
  * Install and start applications on UEs and remote host
  * The first application is aways the ITS application
  * If the number of beares per UE is more than one,
  * then other types of applications can be used
  */
  uint16_t dlPort = 10000;
  uint16_t ulPort = 20000;

  // randomize a bit start times to avoid simulation artifacts
  // (e.g., buffer overflows due to packet transmissions happening
  // exactly at the same time)
  Ptr<UniformRandomVariable> startTimeSeconds = CreateObject<UniformRandomVariable> ();
  startTimeSeconds->SetAttribute ("Min", DoubleValue (0));
  startTimeSeconds->SetAttribute ("Max", DoubleValue (0.050));

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dis(0, 3);

  uint32_t applications[4];

  uint32_t voip = 0;
  applications[voip] = (numberOfUes * 30) / 100;
  uint32_t web = 1;
  applications[web] = (numberOfUes * 30) / 100;
  uint32_t video = 2;
  applications[video] = (numberOfUes * 20) / 100;
  uint32_t game = 3;
  applications[game] = (numberOfUes * 20) / 100;

  std::vector<std::string> appsS;
  std::string appFileName = "app";
  appFileName +=  std::to_string(numberOfUes) + ".txt";
  std::ifstream is(appFileName);
  std::istream_iterator<int> start(is), end;
  std::vector<int> appsI(start, end);
  if(!first or rerouteType == 1 or updateIntervalTime == 20)
  {
    std::cout << appsI.size() << std::endl;
    for(auto i : appsI)
      std::cout << i << std::endl;
  }
  std::cout << numberOfUes << std::endl;
  for (uint32_t u = 0; u < numberOfUes; ++u)
    {
      Ptr<Node> ue = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ue->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);

      for (uint32_t b = 0; b < numBearersPerUe; ++b)
        {
          uint32_t applicationType;

          ++dlPort;
          ++ulPort;

          ApplicationContainer clientApps;
          ApplicationContainer serverApps;

          if(b == 0)
          {
            uint16_t milisec = 25;
            NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
            UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
            dlClientHelper.SetAttribute ("Interval", TimeValue (MilliSeconds (milisec)));
            dlClientHelper.SetAttribute ("MaxPackets", UintegerValue (1000000000));
            dlClientHelper.SetAttribute ("PacketSize", UintegerValue (400));
            clientApps.Add (dlClientHelper.Install (remoteHost));
            PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                                InetSocketAddress (Ipv4Address::GetAny (), dlPort));
            serverApps.Add (dlPacketSinkHelper.Install (ue));

            NS_LOG_LOGIC ("installing UDP UL app for UE " << u);
            UdpClientHelper ulClientHelper (remoteHostAddr, ulPort);
            ulClientHelper.SetAttribute ("Interval", TimeValue (MilliSeconds (milisec)));
            ulClientHelper.SetAttribute ("MaxPackets", UintegerValue (1000000000));
            ulClientHelper.SetAttribute ("PacketSize", UintegerValue (400));
            clientApps.Add (ulClientHelper.Install (ue));
            PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory",
                                                InetSocketAddress (Ipv4Address::GetAny (), ulPort));
            serverApps.Add (ulPacketSinkHelper.Install (remoteHost));

            vehAppSum[u] = (1000 / milisec) * 400;
          }
          else
          {
            if(first)
            {
              while(true)
              {
                applicationType = dis(gen);
                if (applications[applicationType] > 0) break;
              }
              appsS.push_back(std::to_string(applicationType));
            }
            else
            {
              applicationType = appsI[u];
            }
            if(applicationType == voip)
            {
              NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
              UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
              dlClientHelper.SetAttribute ("Interval", TimeValue (MilliSeconds (20)));
              dlClientHelper.SetAttribute ("MaxPackets", UintegerValue (1000000000));
              dlClientHelper.SetAttribute ("PacketSize", UintegerValue (40));
              clientApps.Add (dlClientHelper.Install (remoteHost));
              PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                                  InetSocketAddress (Ipv4Address::GetAny (), dlPort));
              serverApps.Add (dlPacketSinkHelper.Install (ue));

              vehAppSum[u] += 2000;
              applications[applicationType] -= 1;
            }
            else if (applicationType == web)
            {
              NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
              UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
              dlClientHelper.SetAttribute ("Interval", TimeValue (MilliSeconds (1000)));
              dlClientHelper.SetAttribute ("MaxPackets", UintegerValue (1000000000));
              dlClientHelper.SetAttribute ("PacketSize", UintegerValue (1200));
              clientApps.Add (dlClientHelper.Install (remoteHost));
              PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                                  InetSocketAddress (Ipv4Address::GetAny (), dlPort));
              serverApps.Add (dlPacketSinkHelper.Install (ue));

              vehAppSum[u] += 1200;
              applications[applicationType] -= 1;
            }
            else if (applicationType == video)
            {
              NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
              UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
              dlClientHelper.SetAttribute ("Interval", TimeValue (MilliSeconds (10)));
              dlClientHelper.SetAttribute ("MaxPackets", UintegerValue (1000000000));
              dlClientHelper.SetAttribute ("PacketSize", UintegerValue (250));
              clientApps.Add (dlClientHelper.Install (remoteHost));
              PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                                  InetSocketAddress (Ipv4Address::GetAny (), dlPort));
              serverApps.Add (dlPacketSinkHelper.Install (ue));

              vehAppSum[u] += 25000;
              applications[applicationType] -= 1;
            }
            else if (applicationType == game)
            {
              NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
              UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
              dlClientHelper.SetAttribute ("Interval", TimeValue (MilliSeconds (20)));
              dlClientHelper.SetAttribute ("MaxPackets", UintegerValue (1000000000));
              dlClientHelper.SetAttribute ("PacketSize", UintegerValue (1200));
              clientApps.Add (dlClientHelper.Install (remoteHost));
              PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                                  InetSocketAddress (Ipv4Address::GetAny (), dlPort));
              serverApps.Add (dlPacketSinkHelper.Install (ue));

              NS_LOG_LOGIC ("installing UDP UL app for UE " << u);
              UdpClientHelper ulClientHelper (remoteHostAddr, ulPort);
              ulClientHelper.SetAttribute ("Interval", TimeValue (MilliSeconds (40)));
              ulClientHelper.SetAttribute ("MaxPackets", UintegerValue (1000000000));
              ulClientHelper.SetAttribute ("PacketSize", UintegerValue (450));
              clientApps.Add (ulClientHelper.Install (ue));
              PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory",
                                                  InetSocketAddress (Ipv4Address::GetAny (), ulPort));
              serverApps.Add (ulPacketSinkHelper.Install (remoteHost));

              vehAppSum[u] += 30000;
              applications[applicationType] -= 1;
            }
          }

          Ptr<EpcTft> tft = Create<EpcTft> ();
          EpcTft::PacketFilter dlpf;
          dlpf.localPortStart = dlPort;
          dlpf.localPortEnd = dlPort;
          tft->Add (dlpf);
          if(b == 0 or (b != 0 and applicationType == game))
          {
            EpcTft::PacketFilter ulpf;
            ulpf.remotePortStart = ulPort;
            ulpf.remotePortEnd = ulPort;
            tft->Add (ulpf);
          }

          EpsBearer bearer ( (b == 0 ? EpsBearer::GBR_GAMING :
                              applicationType == voip ? EpsBearer:: GBR_CONV_VOICE :
                              applicationType == web ? EpsBearer:: NGBR_VIDEO_TCP_DEFAULT :
                              applicationType == video ? EpsBearer:: GBR_CONV_VIDEO :
                              EpsBearer:: GBR_GAMING) );

          lteHelper->ActivateDedicatedEpsBearer (ueLteDevs.Get (u), bearer, tft);

          Time startTime = Seconds (startTimeSeconds->GetValue ());
          /*
             Uses startTimes when vehicles starts moving at any point of the simulation
          */
          // Time startTime = Seconds (startTimes[u] + 1);
          Time stopTime = first ? Seconds (simTime) : Seconds (stopTimes[u] + 3);

          serverApps.Start (startTime);
          serverApps.Stop (stopTime);
          clientApps.Start (startTime);
          clientApps.Stop (stopTime);
        } // end for b
    }
  if(first and rerouteType == 0 and updateIntervalTime == 10)
  {
    std::ofstream outFile(appFileName);
    // the important part
    for (const auto &e : appsS) std::cout << e << "\n";
    for (const auto &e : appsS) outFile << e << "\n";
  }

  // Add X2 interface
  lteHelper->AddX2Interface (enbNodes);

  // X2-based Handover
  //lteHelper->HandoverRequest (Seconds (0.100), ueLteDevs.Get (0), enbLteDevs.Get (0), enbLteDevs.Get (1));

  // Uncomment to enable PCAP tracing
  // p2ph.EnablePcapAll("lena-x2-handover-measures");

  // Tracing of the packets transmitted on the diferent layers
  lteHelper->EnableTraces ();
  // lteHelper->EnablePhyTraces ();
  // lteHelper->EnableMacTraces ();
  // lteHelper->EnableRlcTraces ();
  // lteHelper->EnablePdcpTraces ();

  Ptr<RadioBearerStatsCalculator> rlcStats = lteHelper->GetRlcStats ();
  rlcStats->SetAttribute ("EpochDuration", TimeValue (Seconds(1.0)));
  Ptr<RadioBearerStatsCalculator> pdcpStats = lteHelper->GetPdcpStats ();
  pdcpStats->SetAttribute ("EpochDuration", TimeValue (Seconds (1.0)));

  // connect custom trace sinks for RRC connection establishment and handover notification
  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/ConnectionEstablished",
                   MakeCallback (&NotifyConnectionEstablishedEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionEstablished",
                   MakeCallback (&NotifyConnectionEstablishedUe));
  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverStart",
                   MakeCallback (&NotifyHandoverStartEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverStart",
                   MakeCallback (&NotifyHandoverStartUe));
  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverEndOk",
                   MakeCallback (&NotifyHandoverEndOkEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverEndOk",
                   MakeCallback (&NotifyHandoverEndOkUe));
  // Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange",
  //                  MakeBoundCallback (&CourseChange, &os));

  /*
     Enable to create a animation file for the transmission of packets
  */
  // AnimationInterface anim ("/home/matheus/workspace/ns-allinone-3.29/ns-3.29/scratch/animation.xml");
  // anim.SetMobilityPollInterval(Seconds(0.25));
  // anim.EnablePacketMetadata(true);

  // Monitor for the flow of packets
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor;
  monitor = flowmon.Install(ueNodes);
  monitor = flowmon.Install(remoteHostContainer);
  monitor = flowmon.GetMonitor();
  // monitor->SetAttribute("DelayBinWidth", DoubleValue(0.001));
  // monitor->SetAttribute("JitterBinWidth", DoubleValue(0.001));
  // monitor->SetAttribute("PacketSizeBinWidth", DoubleValue(20));

  // callback function for node creation
  std::function<Ptr<Node> ()> setupNewWifiNode = [&] () -> Ptr<Node>
  {
    if (nodeCounter >= ueNodes.GetN())
      NS_FATAL_ERROR("Node Pool empty!: " << nodeCounter << " nodes created.");

    // don't create and install the protocol stack of the node at simulation time -> take from "node pool"
    Ptr<Node> includedNode = ueNodes.Get(nodeCounter);
    ++nodeCounter;// increment counter for next node
    std::cout << "Node Id to connect: " << includedNode->GetId() << std::endl;
    return includedNode;
  };

  // callback function for node shutdown
  std::function<void (Ptr<Node>)> shutdownWifiNode = [&] (Ptr<Node> exNode)
  {
    std::cout << "Node " << exNode->GetId() << " chegou" << std::endl;
    // Detach node in LTE scenario
    desconnectNode(exNode);
  };

  // callback function for node connection to eNB tower
  std::function<void()> connectNode = [&] ()
  {
    lteHelper->AttachToClosestEnb (ueLteDevs.Get(nodeCounter - 1), enbLteDevs);
  };

  if(!dlPdcpOutputFilename.empty()) pdcpStats->SetDlPdcpOutputFilename(dlPdcpOutputFilename);
  if(!ulPdcpOutputFilename.empty()) pdcpStats->SetUlPdcpOutputFilename(ulPdcpOutputFilename);
  if(!dlRlcOutputFilename.empty()) rlcStats->SetDlOutputFilename(dlRlcOutputFilename);
  if(!ulRlcpOutputFilename.empty()) rlcStats->SetUlOutputFilename(ulRlcpOutputFilename);

  /*
  * Start traci client with given function pointers
  * the first parameter is the callback function for node creation
  * the second parameter is the callback function for node shutdown
  * the third parameter is the data structure with the amount of bytes transmitted by each tower
  * the fourth parameter is the callback function for node connection to eNB tower
  * the fifth parameter is the callback function for node creation
  * the sexth parameter is the data structure with the amount of bytes transmitted by all the applications from each vehicle
  */
  sumoClient->SumoSetup (setupNewWifiNode, shutdownWifiNode, pdcpStats->m_lastStats, connectNode, vehAppSum);

  Simulator::Schedule(Seconds(10), &ThroughputMonitor, &flowmon, monitor);

  Simulator::Stop (Seconds (simTime));
  Simulator::Run ();

  monitor->CheckForLostPackets ();
  monitor->SerializeToXmlFile ("results.xml" , true, true );
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
  {
  Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

          NS_LOG_UNCOND("Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress);
          NS_LOG_UNCOND("Duration: " << iter->second.timeLastTxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds());
          NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
          NS_LOG_UNCOND("Tx Bytes = " << iter->second.txBytes);
          NS_LOG_UNCOND("Sum jitter = " << iter->second.jitterSum);
          NS_LOG_UNCOND("Delay Sum = " << iter->second.delaySum);
          NS_LOG_UNCOND("Lost Packet = " << iter->second.lostPackets);
          NS_LOG_UNCOND("Dropped Packet = " << iter->second.packetsDropped.size());
          NS_LOG_UNCOND("Rx Bytes = " << iter->second.rxBytes);
          NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
          NS_LOG_UNCOND("Last Received Packet: "<< iter->second.timeLastTxPacket.GetSeconds()<<" Seconds");
          NS_LOG_UNCOND("Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstRxPacket.GetSeconds()) / 1048576  << " Mbps");
          NS_LOG_UNCOND("Packet loss %= " << ((iter->second.txPackets-iter->second.rxPackets)*1.0)/iter->second.txPackets);
  }

  // GtkConfigStore config;
  // config.ConfigureAttributes ();
  // monitor->SerializeToXmlFile ("verb300.tr" , false, false);

  Simulator::Destroy ();
  return 0;
}