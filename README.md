### Descrição
A implementação desse módulo do ns3 é uma modificação do acoplador bidirecional [ns3-sumo-coupling](https://software.dlr.de/p/sumo/home/), que faz a ligação entre o [SUMO](https://software.dlr.de/p/sumo/home/) e o ns3 através da API [TraCI API](http://sumo.dlr.de/wiki). O desenvolvimento desse módulo foi voltado para o uso do SUMO no contexto de ITS em conjunto com a forma de comunicação LTE do ns3. Esse módulo possui como pré-requisitos a instalação do SUMO versão 1.7.0 e o uso do ns3 na versão 3.30.

### Uso
Apague o diretório `lte/` que está dentro do diretório `<ns3-root>/src/` para poder utilizar a versão modificada que está dentror desse repositório. Faça o clone do repositório de forma que o conteúdo dele fique diretamente de `<ns3-root>/src/`.
```sh
$ cd <ns3-root>/src/
$ git init .
$ git remote add -t \* -f origin https://matheus-bernardino@bitbucket.org/matheus-bernardino/sumo-ns3.git
$ git checkout master
```
Build/rebuild o seu simulador ns3 com o novo módulo e execute o exemplo para verificação.
```sh
$ cd <ns3-root>/
$ ./waf configure --build-profile=debug --enable-examples --enable-tests
$ ./waf build
```
Para rodar as simulações são utilizados scripts que facilitam a passagem de parâmetros para a simulação e também executam cada cenário duas vezes seguidas para que, durante a primeira execução, sejam descobertos os momentos em que os veículos terminaram as suas rotas e, durante a segunda execução, as aplicações que rodando em cada veículo parem de enviar pacotes no momento em que os veículos concluem as suas rotas. Como forma de teste, o script `<ns3-root>/src/traci/examples/grid/7x7_160_Veiculos/exe7x7.160.10.0.sh` pode ser copiado para para `<ns3-root>` e então ser executado para inicializar a simulação de um cenário de grid 7x7 com 160 veículos.
```sh
$ cd <ns3-root>/
$ cp src/traci/examples/grid/7x7_160_Veiculos/exe7x7.160.10.0.sh .
$ ./exe7x7.160.10.0.sh
```
Para vixualizar o cenário no ns3, use a flag `--vis` na execução do script
Para vixualizar o cenário no ns3 em conjunto com o `sumo-gui`, configure o atributo no exemplo `<ns3-root>/src/traci-applications/examples/lte.cc` para
```
client->SetAttribute("SumoGUI", BooleanValue(true));
```

### Especificações do sistema
Esse módulo foi testado no seguinte sistema:
* ns3: 3.30
* SUMO: 1.7.0
* Ubuntu: 18.04 LTS
* gcc: 7.5.0

### References
[1] Y. Pigné, G. Danoy & P. Bouvry, "A platform for realistic online vehicular network management," 2010 IEEE Globecom Workshops, Miami, FL, 2010, pp. 595-599.

[2] Wegener, Axel & Piorkowski, Michal & Raya, Maxim & Hellbrück, Horst & Fischer, Stefan & Hubaux, Jean-Pierre. (2008). TraCI: An Interface for Coupling Road Traffic and Network Simulators. Proceedings of the 11th Communications and Networking Simulation Symposium, CNS'08.


### Authors
Matheus Bernardino Araújo, UECE, LARCES, <matheus.bernardino@larces.uece.br>

Matheus Monteiro Silveira, UECE, LARCES, <matheus.monteiro@larces.uece.br>